export interface ITodoModel {
  name: string;
}

export interface ITodoModelWithId extends ITodoModel {
  id: string;
}
